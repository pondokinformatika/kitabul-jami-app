import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen';

import Home from './screens/Home';
import Detail from './screens/Detail';
import About from './screens/About';

const RootStack = createStackNavigator(
  {
    Home: {
      screen: Home,
    },
    Detail: {
      screen: Detail,
    },
    About: {
      screen: About,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends Component {

  componentDidMount() {
    SplashScreen.hide()
  }

  render() {
    return (
      <RootStack />
    );
  }
}