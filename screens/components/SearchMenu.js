import React, { Component } from 'react';

import { StyleSheet, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { View, Text, List, ListItem } from 'native-base';

const SearchMenu = (props) => {
  const { hide, data, navigation } = props;

  if (hide) {
    return null;
  }
  return (
    <View style={styles.float}>
      <List>
        <FlatList
          data={data}
          keyExtractor={(item) => item.id}
          renderItem={({item}) =>
            <ListItem style={styles.textFloat}>
              <TouchableOpacity style={styles.button} onPress={() => 
                {
                  navigation.setParams({
                    hideSetting: true,
                    hideSearch: true,
                  });
                  navigation.navigate('Detail', {id: item.id});
                }
              }>
                <Text style={styles.text}>{item.judul}</Text>
              </TouchableOpacity>
            </ListItem>
          }
        />
      </List>
    </View>
  );
}

const styles = StyleSheet.create({
  float: {
    position: 'absolute',
    backgroundColor: '#fff',
    width: Dimensions.get('window').width - 30,
    alignSelf: 'center',
    elevation: 2,
    top: -25,
  },
  textFloat: {
    marginLeft: 0,
    paddingTop: 0,
    paddingBottom: 0,
    paddingRight: 0,
  },
  button: {
    paddingVertical: 15,
    flex: 1,
  },
  text: {
    fontFamily: 'SourceSansPro',
    fontStyle: 'italic',
    fontSize: 14,
    alignSelf: 'flex-start',
    paddingHorizontal: 10,
  }
});

export default SearchMenu;