import React, { Component } from 'react';

import { StyleSheet, View, FlatList, Text, Dimensions, ScrollView, TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";

import isiHadits from '../data/isiHadits.json';

import OptionMenu from './components/OptionMenu';

class Detail extends Component {

	constructor(props) {
		super(props);

		const id = this.props.navigation.getParam('id');
		const index = id - 1;
	  const bab = id <= 16 ? 1 :
								id <= 30 ? 2 :
								id <= 41 ? 3 :
								id <= 96 ? 4 :
								id <= 131 ? 5 :
								null;
		const haditsLink = `Download Aplikasi Kitabul Jami' di Playstore https://play.google.com/store/apps/details?id=com.kitab.jami`;
	  const haditsJudul = isiHadits[index].judul;
	  const haditsArab = isiHadits[index].arab;
		const haditsTerjemahan = isiHadits[index].terjemahan;
		const haditsFootnote = isiHadits[index].sumber;
		const haditsAll = haditsJudul + "\n \n" + haditsArab + "\n \n" + haditsTerjemahan + "\n \n" + haditsFootnote;

		this.props.navigation.setParams({
			bab: bab,
			haditsArab: haditsJudul + "\n \n" + haditsArab + "\n \n" + haditsLink,
			haditsTerjemahan: haditsJudul + "\n \n" + haditsTerjemahan + "\n \n" + haditsLink,
			haditsAll: haditsAll + "\n \n" + haditsLink,
		});
	}

	// ---- HEADER LAYAR DETAIL / NOMOR HADIS, BAB, DAN TOMBOL OPTION ---- //
	static navigationOptions = ({navigation}) => {

		const paramId = navigation.getParam('id');
		const paramBab = navigation.getParam('bab');

    // **** YANG MENGATUR SHOW / HIDE OPTION MENU **** //
		optionButtonHandler = () => {
      id = navigation.getParam('id');
      index = id - 1;
      hideOption = navigation.getParam('hideOption', true);

			haditsLink = `Download Aplikasi Kitabul Jami' di Playstore https://play.google.com/store/apps/details?id=com.kitab.jami`;
		  haditsJudul = isiHadits[index].judul;
		  haditsArab = isiHadits[index].arab;
			haditsTerjemahan = isiHadits[index].terjemahan;
			haditsFootnote = isiHadits[index].sumber;
			haditsAll = haditsJudul + "\n \n" + haditsArab + "\n \n" + haditsTerjemahan + "\n \n" + haditsFootnote;
      
      navigation.setParams({ 
      	hideOption: !hideOption,
				haditsArab: haditsJudul + "\n \n" + haditsArab + "\n \n" + haditsLink,
				haditsTerjemahan: haditsJudul + "\n \n" + haditsTerjemahan + "\n \n" + haditsLink,
				haditsAll: haditsAll + "\n \n" + haditsLink,
      });
    };

		detailBackButton = (
			<TouchableOpacity
			  onPress={() => navigation.goBack(null)}
			  style={styles.headerButton}
			>
				<Icon name="arrow-back" style={styles.headerIcon}/>
			</TouchableOpacity>
		);

		optionButton = (
			<TouchableOpacity
			  style={styles.headerButton}
			  onPress={optionButtonHandler}
			>
				<Icon name="more-vert" style={styles.headerIcon}/>
			</TouchableOpacity>
		);

		return {
			headerTitle: (<Text style={styles.headerTitle}>HADITS {paramId} | BAB {paramBab}</Text>),
			headerLeft: detailBackButton,
			headerRight: optionButton,
			headerStyle: {
				backgroundColor: 'rgba(0,0,0,0)',
				height: 80,
				elevation: 0,
			},
			headerTitleStyle: {
        marginHorizontal: 0,
			},
			headerTintColor: '#2C3445',
		}
	};

	// ---- FUNCTION KETIKA MENGGESER HALAMAN ---- //
	handleScroll(event) {
		const id = event.nativeEvent.contentOffset.x / Dimensions.get('window').width + 1;
		const bab = id <= 16 ? 1 :
								id <= 30 ? 2 :
								id <= 41 ? 3 :
								id <= 96 ? 4 :
								id <= 131 ? 5 :
								null;

		this.props.navigation.setParams({
			hideOption: true,
			id: parseInt(id),
			bab: bab,
		});
	}

	// ---- BODY ---- //
  render() {
  	const ITEM_WIDTH = Dimensions.get('window').width; 
    return (
  		<View style={styles.container}>
    		{/* **** FLATLIST BESERTA KONFIGURASINYA **** */}
				<FlatList
					horizontal
					showsHorizontalScrollIndicator={false}
					pagingEnabled={true}
					data={isiHadits}
					keyExtractor={(item) => item.id}
					getItemLayout={(data, index) => (
				    {length: ITEM_WIDTH, offset: ITEM_WIDTH * index, index}
				  )}
				  initialScrollIndex={this.props.navigation.getParam('id') - 1}
				  onMomentumScrollEnd={(event) => this.handleScroll(event)}
					renderItem={({item}) =>
						<ScrollView>
							{/* **** DETAIL HADITS **** */}
    					<View style={styles.wrapper}>
		      			<Text style={[styles.judul, styles.textdetail]}>{item.judul}</Text>
		      			<Text style={styles.arab}>{item.arab}</Text>
		      			<Text style={[styles.terjemahan, styles.textdetail]}>{item.terjemahan}</Text>
		      			<Text style={styles.sumber}>{item.sumber}</Text>
    					</View>
	      		</ScrollView>
					}
				/>

				<OptionMenu
          hide={this.props.navigation.getParam('hideOption', true)}
          navigation={this.props.navigation}
        /> 

  		</View>
    );
  }
}

const styles = StyleSheet.create({
	headerTitle: {
		fontFamily: 'SourceSansPro',
		fontSize: 17,
		fontWeight: 'bold',
		flex: 1,
		textAlign: 'center',
		color: '#2C3445',
	},
	headerButton: {
    padding: 13,
	},
	headerIcon: {
    color: '#2C3445',
    fontSize: 22,
	},
	container: {
	},
	wrapper: {
		backgroundColor: '#fff',
		width: Dimensions.get('window').width - 30,
		alignSelf: 'center',
		paddingTop: 30,
		paddingHorizontal: 15,
		margin: 15,
		marginTop: 0,
	},
	textdetail: {
		color: '#2C3445',
		fontFamily: 'SourceSansPro',
	},
	judul: {
		alignSelf: 'center',
		fontSize: 18,
		marginBottom: 30,
		marginTop: 10,
		textAlign: 'center',
		lineHeight: 30,
	},
	arab: {
		color: '#2C3445',
		fontSize: 30,
		marginBottom: 40,
		lineHeight: 50,
    fontFamily: 'Scheherazade',
    // fontWeight: 'bold',
	},
	terjemahan: {
		fontSize: 16,
		fontStyle: 'italic',
		lineHeight: 24,
		marginBottom: 20,
	},
	sumber: {
		fontSize: 11,
		lineHeight: 20,
		marginBottom: 20,
		color: '#888',
		fontFamily: 'SourceSansPro',
	},
});

export default Detail;