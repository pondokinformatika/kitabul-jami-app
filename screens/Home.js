import React, { Component } from "react";
import { TouchableOpacity, StyleSheet, FlatList, ScrollView, Dimensions } from "react-native";
import { View, Text, Item, Input } from "native-base";
import Icon from "react-native-vector-icons/MaterialIcons";

import daftarHadits from '../data/daftarHadits.json';
import isiHadits from '../data/isiHadits.json';

import SearchMenu from './components/SearchMenu';
import SettingMenu from './components/SettingMenu';

class Home extends Component {

  babButtonHandler(id){
    this.props.navigation.setParams({
      hideSetting: true,
      hideSearch: true,
      isOpen: this.props.navigation.getParam('isOpen', false) == id ? false : id,
    });
    this.scroller.scrollTo({x: 0, y: (101.7 * id) - 101.7});
  }

  haditsButtonHandler(id){
    this.props.navigation.navigate('Detail', {id: id});
  }

  // =============== HEADER LAYAR HOME =============== //
  static navigationOptions = ({navigation}) => {
    
    // ---- FUNCTION YANG MENGATUR FITUR PENCARIAN ---- //
    dataHolder = isiHadits;
    searchHandler = (text) => {

      // **** YANG MENGATUR SHOW / HIDE SEARCH MENU **** //
      hideSearch = navigation.getParam('hideSearch', true);
      if (text) {
        if (hideSearch == true) {
          navigation.setParams({
            hideSearch: false,
          });
        }
      } else {
        navigation.setParams({
          hideSearch: true,
        });
      }

      // **** YANG MENGATUR ISI DARI SEARCH MENU **** //
      const newData = dataHolder.filter((item) => { 
        const itemData = item.judul.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      navigation.setParams({isiHadits: newData.slice(0,5)});
    };

    settingButtonHandler = () => {
      hideSetting = navigation.getParam('hideSetting', true);
      navigation.setParams({ hideSetting: !hideSetting });
    };

    // ---- KETIKA TOMBOL SEARCH DI KLIK / ISI DARI SEARCH INPUT ---- //
    searchButtonHandler = () => {
      navigation.setParams({
        headerTitle: (
          <Item style={styles.searchInputWrapper}>
            <Input 
              placeholder="Search..."
              placeholderTextColor="#666"
              style={styles.searchInput}
              onChangeText={searchHandler}
            />
          </Item>
        ),
        headerLeft: closeButton,
        headerRight: null,
      })
    };

    // ---- KETIKA TOMBOL BACK DI KLIK / KEMBALI KE HEADER SEMULA ---- //
    closeButtonHandler = () => {
      navigation.setParams({
        headerTitle: "KITABUL JAMI'",
        headerLeft: settingButton,
        headerRight: searchButton,
        hideSearch: true,
      });
    };

    // ---- TOMBOL BACK DARI KEADAAN SEARCH ---- //
    closeButton = (
      <TouchableOpacity
        style={styles.headerButton}
        onPress={closeButtonHandler}
      >
        <Icon name='clear' style={styles.headerIcon}/>
      </TouchableOpacity>
    );

    // ---- TOMBOL SEARCH ---- //
    searchButton = (
      <TouchableOpacity 
        style={styles.headerButton}
        onPress={searchButtonHandler}
      >
        <Icon name='search' style={styles.headerIcon}/>
      </TouchableOpacity>
    );

    settingButton = (
      <TouchableOpacity 
        style={styles.headerButton}
        onPress={settingButtonHandler}
      >
        <Icon name='menu' style={styles.headerIcon}/>
      </TouchableOpacity>
    );

    // ---- KEADAAN DEFAULT HEADER HOME ---- //
    return {
      headerTitle: navigation.getParam('headerTitle', "KITABUL JAMI'"),
      headerLeft: navigation.getParam('headerLeft', settingButton),
      headerRight: navigation.getParam('headerRight', searchButton),
      headerStyle: {
        backgroundColor: 'rgba(0,0,0,0)',
        elevation: 0,
        height: 80,
      },
      headerTitleStyle: {
        fontFamily: 'SourceSansPro',
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontSize: 17,
        marginHorizontal: 0,
        flex: 1,
        textAlign: 'center',
        letterSpacing: 2,
      },
      headerTintColor: '#2C3445',
    }
  };

  // =============== BODY HOME =============== //

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollView} ref={(scroller) => {this.scroller = scroller}}>
          {daftarHadits.map((bab) =>
            <View key={bab.id} style={styles.babWrapper}>
              <TouchableOpacity
                style={styles.babButton}
                onPress={(id) => this.babButtonHandler(bab.id)}
              >
                <View style={styles.babTitleWrapper}>
                  <Icon style={styles.babIcon} name="spa"/>
                  <Text style={styles.babTitle}>Bab {bab.judul}</Text>
                  <Icon style={styles.babIcon} name="spa"/>
                </View>
              </TouchableOpacity>
              <View style={[styles.haditsWrapper, (this.props.navigation.getParam('isOpen', false) == bab.id ? styles.haditsWrapperOpen : null)]}>
                {bab.hadits.map((hadits) =>
                  <TouchableOpacity 
                    key={hadits.id}
                    style={styles.haditsButton}
                    onPress={(id) => this.haditsButtonHandler(hadits.id)}
                  >
                    <View style={styles.haditsTitleWrapper}>
                      <Text style={styles.haditsTitle}>{hadits.judul}</Text>
                      <View style={styles.haditsNumber}>
                        <Text style={styles.badge}>{hadits.id}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                )}
              </View>
              <View style={styles.ornsWrapper}>
                <View style={[styles.orn, styles.ornLineLeft]}></View>
                <View style={[styles.orn, styles.ornDot]}></View>
                <View style={[styles.orn, styles.ornDot]}></View>
                <View style={[styles.orn, styles.ornDot]}></View>
                <View style={[styles.orn, styles.ornLineRight]}></View>
              </View>
            </View>
          )}
        </ScrollView>

        {/* **** SEARCH MENU (FILE TERPISAH, DI FOLDER COMPONENTS) **** */}
        <SearchMenu
          hide={this.props.navigation.getParam('hideSearch', true)}
          data={this.props.navigation.getParam('isiHadits', isiHadits)}
          navigation={this.props.navigation}
        />

        {/* **** SETTING MENU (FILE TERPISAH, DI FOLDER COMPONENTS) **** */}
        <SettingMenu
          hide={this.props.navigation.getParam('hideSetting', true)}
          navigation={this.props.navigation}
        /> 
      </View>
    );
  }
}

// =============== STYLE HOME =============== //

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
  },
  headerButton: {
    padding: 13,
  },
  headerIcon: {
    color: '#2C3445',
    fontSize: 22,
  },
  searchInput: {
    paddingBottom: 3,
    height: 30,
    width: 100,
    color: '#2C3445',
    fontFamily: 'SourceSansPro',
    fontStyle: 'italic',
  },
  searchInputWrapper: {
    marginTop: -2,
    marginRight: 20,
    borderBottomColor: '#2C3445',
  },
  text: {
    fontSize: 15,
    color: '#2C3445'
  },
  babWrapper: {
    backgroundColor: "#FFF",
    marginHorizontal: 15,
    marginBottom: 20,
    position: 'relative',
  },
  babButton: {
    paddingVertical: 30,
    justifyContent: "center", 
    alignItems: "center", 
    backgroundColor: "#FFF",
  },
  babTitleWrapper: {
    flexDirection: 'row',
  },
  babTitle: {
    flex: 6,
    fontFamily: 'SourceSansPro',
    fontSize: 17,
    color: '#2C3445',
    textAlign: 'center',
  },
  babIcon: {
    flex: 1,
    textAlign: 'center',
    fontSize: 15,
    color: 'rgba(120,120,120,0.5)',
    marginTop: 5,
  },
  haditsWrapper: {
    display: 'none',
    backgroundColor: '#FFF',
  },
  haditsWrapperOpen: {
    display: 'flex', 
    borderTopWidth: 2, 
    borderTopColor: 'rgba(120,120,120,0.4)', 
    borderStyle: 'solid'
  },
  haditsButton: {

  },
  haditsTitleWrapper: {
    paddingVertical: 15,
    marginHorizontal: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    borderStyle: 'solid',
    flexDirection: 'row',
  },
  haditsTitle: {
    flex: 7,
    fontFamily: 'SourceSansPro',
    fontStyle: 'italic',
    color: '#2C3445',
  },
  haditsNumber: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  badge: {
    backgroundColor: '#5C6962',
    color: '#FFF',
    textAlign: 'center',
    width: 25,
    height: 20,
    paddingVertical: 1.5,
    fontFamily: 'SourceSansPro',
    fontSize: 12,
  },
  ornsWrapper: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  orn: {
    backgroundColor: 'rgba(120,120,120,0.4)',
    marginTop: 5,
  },
  ornLineLeft: {
    height: 2,
    width: '45%',
    marginRight: 1,
  },
  ornLineRight: {
    height: 2,
    width: '45%',
    marginLeft: 1,
  },
  ornDot: {
    height: 4,
    width: 4,
    borderRadius: 4,
    marginHorizontal: 1,
  }
});

export default Home;