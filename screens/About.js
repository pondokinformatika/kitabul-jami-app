import React, { Component } from 'react';

import { StyleSheet, Dimensions, ScrollView, Image, TouchableOpacity } from 'react-native';
import { View, Text } from "native-base";
import Icon from "react-native-vector-icons/MaterialIcons";

class About extends Component {

	static navigationOptions = ({navigation}) => {

    // ---- TOMBOL BACK DARI KEADAAN SEARCH ---- //
    aboutBackButton = (
			<TouchableOpacity
			  onPress={() => navigation.goBack(null)}
			  style={styles.headerButton}
			>
				<Icon name="arrow-back" style={styles.headerIcon}/>
			</TouchableOpacity>
		);

    // ---- KEADAAN DEFAULT HEADER HOME ---- //
    return {
      headerTitle: "ABOUT",
      headerLeft: aboutBackButton,
      headerRight: (<View style={styles.headerButton}></View>),
      headerStyle: {
        backgroundColor: 'rgba(0,0,0,0)',
        elevation: 0,
        height: 80,
      },
      headerTitleStyle: {
        fontFamily: 'SourceSansPro',
        fontWeight: 'bold',
        fontSize: 17,
        marginHorizontal: 0,
        flex: 1,
        textAlign: 'center',
        letterSpacing: 1,
      },
      headerTintColor: '#2C3445',
    }
	};

	// ---- BODY ---- //
  render() {
    return (
    	<View style={styles.container}>
				<ScrollView>
					<View style={styles.wrapper}>
						<Image
						  style={styles.logo}
						  source={require('../assets/img/logo-kitab.png')}
						/>
						<Text style={[styles.mainTitle, styles.text]}>KITABUL JAMI'</Text>
						<Text style={[styles.subTitle, styles.text]}>by Pondok Informatika Al-Madinah</Text>
						<Text style={[styles.paragraph, styles.text]}>
							Kitabul Jami’ merupakan Bab terakhir dari Bulughul Maram yang berisi 131 hadis berkenaan dengan Adab, Akhlaq dan Dzikir serta Do‘a sehari-hari seorang muslim.
						</Text>
						<Text style={[styles.paragraph, styles.text]}>
							Project ini merupakan hasil pembelajaran mobile application dari santri jurusan programming di Pondok Informatika Al-Madinah. Dikerjakan dengan basis React Native selama 1 bulan ½.
						</Text>
						<Text style={[styles.paragraph, styles.text]}>
							Teks arab disadur dari sunnah.com, juga terimakasih kepada kitabuljami.wordpress.com untuk terjemahan bahasa indonesia.
						</Text>
            <Text style={[styles.paragraph, styles.text]}>
              Ucapan terimakasih juga kepada tim website wahdah.or.id yang telah membantu distribusi dan penyebaran aplikasi ini.
            </Text>
						<Text style={[styles.paragraph, styles.text]}>
							Semoga aplikasi ini dapat memberikan banyak manfaat bagi para penggunanya. Tentunya dalam pengembangan aplikasi ini masih banyak kekurangan oleh karena itu, masukan dan kritikan dari para pengguna sangat kami harapkan.
						</Text>
            <View style={styles.contact}>
              <Text style={[styles.contactHeader, styles.text]}>Developers</Text>
              <View style={styles.contactItem}>
                <Icon name="navigate-next" style={styles.contactIcon}/>
                <Text style={[styles.contactText, styles.text]}>Irhamullah Yunta S.Kom</Text>
              </View>
              <View style={styles.contactItem}>
                <Icon name="navigate-next" style={styles.contactIcon}/>
                <Text style={[styles.contactText, styles.text]}>Yahya</Text>
              </View>
              <View style={styles.contactItem}>
                <Icon name="navigate-next" style={styles.contactIcon}/>
                <Text style={[styles.contactText, styles.text]}>Muhammad Abid Khairy</Text>
              </View>
              <View style={styles.contactItem}>
                <Icon name="navigate-next" style={styles.contactIcon}/>
                <Text style={[styles.contactText, styles.text]}>Abdullah Ammar Al-Faruq</Text>
              </View>
              <View style={styles.contactItem}>
                <Icon name="navigate-next" style={styles.contactIcon}/>
                <Text style={[styles.contactText, styles.text]}>Muhammad</Text>
              </View>
              <View style={styles.contactItem}>
                <Icon name="navigate-next" style={styles.contactIcon}/>
                <Text style={[styles.contactText, styles.text]}>Bilal</Text>
              </View>
            </View>
						<View style={styles.contact}>
							<Text style={[styles.contactHeader, styles.text]}>Pondok Informatika Al-Madinah</Text>
							<View style={styles.contactItem}>
								<Icon name="mail-outline" style={styles.contactIcon}/>
								<Text style={[styles.contactText, styles.text]}>pondokitalmadinah@gmail.com</Text>
							</View>
							<View style={styles.contactItem}>
								<Icon name="public" style={styles.contactIcon}/>
								<Text style={[styles.contactText, styles.text]}>http://pondokinformatika.com</Text>
							</View>
							<View style={styles.contactItem}>
								<Icon name="phone-iphone" style={styles.contactIcon}/>
								<Text style={[styles.contactText, styles.text]}>0857 2524 9265 / 0822 5718 2656 (Irhamullah)</Text>
							</View>
							<View style={styles.contactItem}>
								<Icon name="home" style={styles.contactIcon}/>
								<Text style={[styles.contactText, styles.text]}>Jl. Raya Krapyak RT.05, Karanganyar, Wedomartani, Ngemplak, Sleman, Daerah Istimewa Yogyakarta</Text>
							</View>
						</View>
						<View style={styles.vendorWrapper}>
							<View style={styles.vendor}>
								<Image
                  style={[styles.vendorImage, {height: 25}]}
                  source={require('../assets/img/logo-pondok.png')}
                />
                <Image
								  style={[styles.vendorImage, {height: 30}]}
								  source={require('../assets/img/logo-wahdah.png')}
								/>
							</View>
						</View>
					</View>
	  		</ScrollView>
    	</View>
    );
  }
}

const styles = StyleSheet.create({
	headerButton: {
    padding: 13,
  },
  headerIcon: {
    color: '#2C3445',
    fontSize: 22,
  },
  wrapper: {
		width: Dimensions.get('window').width - 30,
		backgroundColor: '#fff',
		alignSelf: 'center',
		paddingVertical: 40,
		paddingHorizontal: 15,
		margin: 15,
		marginTop: 0,
  },
  logo: {
  	width: 130,
  	height: 130,
  	alignSelf: 'center',
  	marginBottom: 10,
  },
  text: {
    color: '#2C3445',
    fontFamily: 'SourceSansPro',
  },
  mainTitle: {
    fontSize: 20,
    fontStyle: 'italic',
    fontWeight: 'bold',
    alignSelf: 'center',
    // marginBottom: 5,
    letterSpacing: 2,
  },
  subTitle: {
    fontSize: 13,
    fontStyle: 'italic',
    alignSelf: 'center',
    marginBottom: 30,
  },
  paragraph: {
    fontSize: 15,
    fontStyle: 'italic',
    marginBottom: 10,
  },
  contact: {
  	marginVertical: 10,
  },
  contactHeader: {
  	marginBottom: 5,
  	fontSize: 15,
  },
  contactItem: {
  	flexDirection: 'row',
  	marginBottom: 2,
  },
  contactIcon: {
  	flex: 1,
    color: '#777',
  	fontSize: 14,
  	paddingTop: 2,
  },
  contactText: {
  	flex: 12,
  	fontSize: 14,
  	fontStyle: 'italic',
  },
  vendorWrapper: {
  	flexDirection: 'row',
  	marginTop: 20,
  },
  vendorTitle: {
    fontSize: 13,
    fontStyle: 'italic',
    alignSelf: 'center',
    marginBottom: 5,
  },
  vendor: {
  	flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  vendorImage: {
    width: 120,
  	alignSelf: 'center',
  },
});

export default About;